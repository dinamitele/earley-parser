#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<stack>
#include <ctype.h>
using namespace std;

struct gramatica
{
	vector<char> T;
	vector<char> N;
	vector<vector<string>> S;// S[0] - configuratia 0 care contine mai multe siruri de caractere
	vector<string> P;
};

struct DatePunct
{
	char caracter;
	int poz;
};


void citesteReguli(gramatica &G)
{
	ifstream file;

	file.open("dateGramatica.txt");
	string line;

	while (getline(file, line))
	{
		if (line == "neterminali")
		{
			getline(file, line);
			while (line != "------")
			{
				G.N.push_back(line[0]);
				getline(file, line);
			}
		}
		else if (line == "terminali")
		{
			getline(file, line);
			while (line != "------")
			{
				G.T.push_back(line[0]);
				getline(file, line);
			}
		}
		else if (line == "productii")
		{
			getline(file, line);
			while (line != "------")
			{
				G.P.push_back(line);
				getline(file, line);
			}
		}
	}
	file.close();
}

bool VerificareSiExtindere(gramatica &G)
{
	if (G.P.size() == 0 || G.N.size() == 0 || G.T.size() == 0)
	{
		cout << "\nVerificati datele gramaticii.\n";
		return false;
	}
	//extind gramatica
	char primNeterminal = G.N[0];
	string extindere;
	extindere.push_back(primNeterminal);
	extindere.append("'->");
	extindere.push_back(primNeterminal);
	G.P.insert(G.P.begin(), extindere);
	return true;
}

void AfiseazaConfiguratii(gramatica G)
{
	cout << "\n\nConfiguratii: \n";
	for (int i = 0; i < G.S.size(); i++)
	{
		cout << "S" << i << endl;
		for (int j = 0; j < G.S[i].size(); j++)
		{
			cout << G.S[i][j] << endl;
		}
		cout << "-------------------------\n";
	}
}

void PrimaConfiguratie(gramatica &G, string cuv)
{
	char primNeterminal = G.N[0];
	//adaug prima configuratie
	string config1;
	config1.push_back(primNeterminal);
	config1.append("'->.");
	config1.push_back(primNeterminal);
	config1.append(",0");

	vector<string> configCurenta;
	configCurenta.push_back(config1);
	G.S.push_back(configCurenta);

	for (int i = 0; i < cuv.length(); i++)
	{
		vector<string> config;
		G.S.push_back(config);
	}
}

bool Terminal(char c, gramatica G)
{
	for (int i = 0; i < G.T.size(); i++)
	{
		if (c == G.T[i])
			return true;
	}
	return false;
}

bool Neterminal(char c, gramatica G)
{
	for (int i = 0; i < G.N.size(); i++)
	{
		if (c == G.N[i])
			return true;
	}
	return false;
}

bool StareFinala(char c)
{
	if (c == ',' || c=='~')
		return true;
	return false;
}

bool AjungeInLambda(char c, gramatica G, string verificate)
{
	for (int i = 0; i <= G.P.size() - 1; i++)
		if (G.P[i][0] == c && G.P[i][G.P[i].length() - 1] == '~')
			return true;
		else if (G.P[i][0] == c && Neterminal(G.P[i][G.P[i].length() - 1], G) &&
			G.P[i][G.P[i].length() - 2] == '>' && verificate.find(c, 0) == -1)
		{
			verificate += c;
			return AjungeInLambda(G.P[i][G.P[i].length() - 1], G, verificate);
		}

	return false;
}

bool VerificaDacaContine(string stare, int nrConfig, gramatica &G)
{
	for (int i = 0; i < G.S[nrConfig].size(); i++)
		if (stare == G.S[nrConfig][i])
			return true;
	return false;
}

DatePunct ElementDupaPunct(string productie)
{
	DatePunct raspuns;
	for (int i = 0; i < productie.length(); i++)
	{
		if (productie[i] == '.')
		{
			raspuns.caracter = productie[i + 1];
			raspuns.poz = i;
		}
	}
	return raspuns;
}


void Scanare(string stare, int nrConfig, string cuv, gramatica &G)
{
	DatePunct info = ElementDupaPunct(stare);
	if (info.caracter == cuv[nrConfig])
	{
		string stareNoua = stare;
		int index = stareNoua.find(".");
		stareNoua.insert(index + 2, ".");
		stareNoua.erase(index, 1);
		if (!VerificaDacaContine(stareNoua, nrConfig + 1, G))
		{
			G.S[nrConfig + 1].push_back(stareNoua);
		}
	}
}

void Predictie(string stare, int nrConfig, gramatica &G)
{
	DatePunct info = ElementDupaPunct(stare);
	if (Neterminal(info.caracter, G))
	{
		for (int i = 0; i <= G.P.size() - 1; i++)
			if (G.P[i][0] == info.caracter && G.P[i][1] != '\'')
			{
				string stareNoua;
				stareNoua = G.P[i];
				for (int j = 0; j < stareNoua.length(); j++)
					if (stareNoua.at(j) == '>')
						stareNoua.insert(j + 1, ".");
				stareNoua += ",";
				stareNoua += to_string(nrConfig);
				if (!VerificaDacaContine(stareNoua, nrConfig, G))
				{
					G.S[nrConfig].push_back(stareNoua);
				}
			}

		if (AjungeInLambda(info.caracter, G, ""))
		{
			string stareNoua = stare;
			int index = -1;
			for (int j = 0; j < stareNoua.length(); j++)
				if (stareNoua[j] == '.')
				{
					stareNoua.erase(j, 1);
					index = j;
				}
			if (index != -1)
			{
				stareNoua.insert(index + 1, ".");
			}
			int pozitieVirgula = stareNoua.find(",", 0);
			if (!VerificaDacaContine(stareNoua, nrConfig, G))
			{
				G.S[nrConfig].push_back(stareNoua);
			}
		}
	}
}

void Completare(string stare, int nrConfig, gramatica &G)
{
	char neterminal = stare[0];
	int index = stare.find(".");
	char inc = stare[stare.size() - 1];
	int i = inc - '0';
	for (int j = 0; j <= G.S[i].size() - 1; j++)
	{		
		int pozitie = 0;
		while (pozitie != -1)
		{
			pozitie = G.S[i][j].find(neterminal, pozitie + 1);
			if (pozitie != -1)
				if (G.S[i][j][pozitie - 1] == '.')
				{
					string stareNoua = G.S[i][j];
					stareNoua.insert(pozitie + 1, ".");
					stareNoua.erase(pozitie - 1, 1);
					if (!VerificaDacaContine(stareNoua, nrConfig, G))
					{
						G.S[nrConfig].push_back(stareNoua);
					}
				}
		}
	}
}

bool VerificaApartenenta(gramatica G)
{
	char primNeterminal = G.N[0];
	string deCautat;
	deCautat.push_back(primNeterminal);
	deCautat.append("'->");
	deCautat.push_back(primNeterminal);
	deCautat.append(".,0");
	int l = G.S.size();
	for (int i = 0; i < G.S[l - 1].size(); i++)
	{
		if (G.S[l - 1][i].compare(deCautat) == 0)
		{
			return true;
		}
	}
	return false;
}

bool Parseaza(gramatica &G, string cuv)
{
	for (int i = 0; i < G.S.size(); i++)
	{
		for (int j = 0; j < G.S[i].size(); j++)
		{
			char element = ElementDupaPunct(G.S[i][j]).caracter;
			if (Neterminal(element, G))
			{
				Predictie(G.S[i][j], i, G);
			}
			else
			{
				if (Terminal(element, G))
				{
					Scanare(G.S[i][j], i, cuv, G);
				}
			}
			if (StareFinala(element))
			{
				Completare(G.S[i][j], i, G);
			}
		}

		for (int j = 0; j < G.S[i].size(); j++)
		{
			char element = ElementDupaPunct(G.S[i][j]).caracter;

			if (StareFinala(element))
			{
				Completare(G.S[i][j], i, G);
			}
		}
	}
	return VerificaApartenenta(G);
}

int main()
{
	//consideram lambda ca fiind simbolul '~'
	gramatica G;
	citesteReguli(G);
	if (!VerificareSiExtindere(G))
	{
		system("pause");
		return 0;
	}

	cout << "\nIntroduceti cuvantul: \n";
	string cuv;
	cin >> cuv;
	
	PrimaConfiguratie(G, cuv);

	if (Parseaza(G, cuv))
	{
		cout << "Cuvantul apartine gramaticii";
	}
	else
	{
		cout << "Cuvantul nu apartine gramaticii";
	}
	AfiseazaConfiguratii(G);

	system("pause");
	return 0;
}